import safe "base" Data.Int
    ( Int )
import safe "base" Data.Kind
    ( Type )
import safe "base" GHC.Float
    ( Float
    )
import safe "base" GHC.Num
    ( (+), (-), (*) )
import safe "base" GHC.Real
    ( (/) )
import      "gloss" Graphics.Gloss.Data.Color
    ( black
    , red
    )
import safe "gloss" Graphics.Gloss.Data.Display
    ( Display( InWindow )
    )
import      "gloss" Graphics.Gloss.Data.Picture
    ( Picture( Color, ThickCircle, Translate )
    )
import      "gloss" Graphics.Gloss.Interface.Pure.Simulate
    ( simulate )
import safe "base" System.IO
    ( IO )

displayMode :: Display
displayMode = InWindow "My Window" (1000, 700) (10, 10)

-- updates per second of real time
rate :: Int
rate = 24

disk :: Float -> Picture
disk radius = ThickCircle (radius / 2) radius

redDisk :: Picture
redDisk = Color red (disk 25)

type Position :: Type
type Position = (Float,Float)

type Velocity :: Type
type Velocity = (Float,Float)

type State :: Type
type State = (Position,Velocity)

initialState :: State
initialState = ((0,0),(40,80))

displayFunc :: State -> Picture
displayFunc ((x,y),_) = Translate x y redDisk

updateFunc :: Float -> State -> State
updateFunc dt ((x,y),(vx,vy))
    = (( x + vx * dt, y +  vy * dt)
      ,(vx          ,vy - 9.8 * dt))

main :: IO ()
main = simulate displayMode black rate initialState displayFunc
       (\_ -> updateFunc)

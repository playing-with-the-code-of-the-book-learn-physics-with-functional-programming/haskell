import safe "base" Data.Kind
    ( Type )
import safe "base" GHC.Float
    ( Double
    )
import      "GLUT" Graphics.UI.GLUT.Objects
    ( Flavour ( Solid ))
import safe "base" System.IO
    ( IO )
import      "not-gloss" Vis
    ( defaultOpts )
import      "not-gloss" Vis.GlossColor
    ( blue )
import      "not-gloss" Vis.Interface
    ( display )
import      "not-gloss" Vis.VisObject
    ( VisObject ( Cube ) )

type R :: Type
type R = Double

blueCube :: VisObject R
blueCube = Cube 1 Solid blue

main :: IO ()
main = display defaultOpts blueCube

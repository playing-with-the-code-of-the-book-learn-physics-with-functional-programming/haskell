import safe "base" Data.Function
    ( ($) )
import      "gloss" Graphics.Gloss.Data.Color
    ( black
    , green
    , red
    )
import safe "gloss" Graphics.Gloss.Data.Display
    ( Display( InWindow )
    )
import      "gloss" Graphics.Gloss.Data.Picture
    ( Picture( Color, Line, Pictures )
    )
import      "gloss" Graphics.Gloss.Interface.Pure.Display
    ( display )
import safe "base" System.IO
    ( IO )


displayMode :: Display
displayMode = InWindow "Axes" (1000, 700) (10, 10)

axes :: Picture
axes = Pictures [Color red   $ Line [(0,0),(100,  0)]
                ,Color green $ Line [(0,0),(  0,100)]
                ]

main :: IO ()
main = display displayMode black axes
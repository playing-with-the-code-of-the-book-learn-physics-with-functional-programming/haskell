import safe "base" GHC.Float
    ( Float )
import safe "base" GHC.Real
    ( (/) )
import      "gloss" Graphics.Gloss.Data.Color
    ( black
    , blue
    , red
    )
import safe "gloss" Graphics.Gloss.Data.Display
    ( Display( InWindow )
    )
import      "gloss" Graphics.Gloss.Data.Picture
    ( Picture( Circle, Color, Pictures, ThickCircle, Translate )
    )
import      "gloss" Graphics.Gloss.Interface.Pure.Display
    ( display )
import safe "base" System.IO
    ( IO )


displayMode :: Display
displayMode = InWindow "My Window" (1000, 700) (10, 10)

blueCircle :: Picture
blueCircle = Color blue (Circle 100)

disk :: Float -> Picture
disk radius = ThickCircle (radius / 2) radius

redDisk :: Picture
redDisk = Color red (disk 100)

wholePicture :: Picture
wholePicture = Pictures [Translate (-120) 0 blueCircle
                        ,Translate   120  0 redDisk
                        ]

main :: IO ()
main = display displayMode black wholePicture
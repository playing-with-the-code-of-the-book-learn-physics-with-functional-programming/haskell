import safe "base" Data.Kind
    ( Type )
import safe "base" Data.Maybe
    ( Maybe ( Nothing ) )
import safe "base" GHC.Float
    ( Double )
import safe "linear" Linear.V3
    ( V3 ( V3 ) )
import safe "base" System.IO
    ( IO )
import      "not-gloss" Vis
    ( defaultOpts )
import      "not-gloss" Vis.GlossColor
    ( blue
    , green
    , red )
import      "not-gloss" Vis.Interface
    ( display )
import      "not-gloss" Vis.VisObject
    ( VisObject ( Line, VisObjects ))

type R :: Type
type R = Double

axes :: VisObject R
axes = VisObjects [Line Nothing [V3 0 0 0, V3 1 0 0] red
                  ,Line Nothing [V3 0 0 0, V3 0 1 0] green
                  ,Line Nothing [V3 0 0 0, V3 0 0 1] blue
                  ]

main :: IO ()
main = display defaultOpts axes

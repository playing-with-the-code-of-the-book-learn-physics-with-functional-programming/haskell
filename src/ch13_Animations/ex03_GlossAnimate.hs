import safe "base" GHC.Float
    ( (**)
    , Float
    )
import safe "base" GHC.Num
    ( (-), (*) )
import safe "base" GHC.Real
    ( (/) )
import      "gloss" Graphics.Gloss.Data.Color
    ( black
    , red
    )
import safe "gloss" Graphics.Gloss.Data.Display
    ( Display( InWindow )
    )
import      "gloss" Graphics.Gloss.Data.Picture
    ( Picture( Color, ThickCircle, Translate )
    )
import      "gloss" Graphics.Gloss.Interface.Pure.Animate
    ( animate )
import safe "base" System.IO
    ( IO )

displayMode :: Display
displayMode = InWindow "My Window" (1000, 700) (10, 10)

disk :: Float -> Picture
disk radius = ThickCircle (radius / 2) radius

redDisk :: Picture
redDisk = Color red (disk 25)

projectileMotion :: Float -> Picture
projectileMotion t = Translate (xDisk t) (yDisk t) redDisk

xDisk :: Float -> Float
xDisk t = 40 * t

yDisk :: Float -> Float
yDisk t = 80 * t - 4.9 * t ** 2

main :: IO ()
main = animate displayMode black projectileMotion

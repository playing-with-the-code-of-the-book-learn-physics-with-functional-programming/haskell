variables:
    BOOTSTRAP_HASKELL_MINIMAL: 'true'
    BOOTSTRAP_HASKELL_NONINTERACTIVE: 'true'
    CABAL_FLAGS: >-
        --disable-optimization
        --ghc-options=-fdiagnostics-color=always
    CABAL_REPL_FLAGS: >-
        $CABAL_FLAGS
        --ghc-options=-fno-warn-missing-local-signatures
    CHRONIC: chronic
    COLUMNS: 50
    TERM: xterm-color
    XDG_CACHE_HOME: $CI_PROJECT_DIR/.cache
    XDG_STATE_HOME: $CI_PROJECT_DIR/.local/state


.ch13-2d:
- time cabal build $CABAL_FLAGS exe:GlossDisplay
- xvfb_runf sh -c "time cabal run $CABAL_FLAGS GlossDisplay & sleep 1 ; xwd -root | xwdtopnm | pnmtopng > GlossDisplay.png"
- file GlossDisplay.png

- time cabal build $CABAL_FLAGS exe:GlossDisplay2
- xvfb_runf sh -c "time cabal run $CABAL_FLAGS GlossDisplay2 & sleep 1 ; xwd -root | xwdtopnm | pnmtopng > GlossDisplay2.png"
- file GlossDisplay2.png

- time cabal build $CABAL_FLAGS exe:GlossAnimate
- xvfb_runf sh -c "time cabal run $CABAL_FLAGS GlossAnimate & sleep 1 ; xwd -root | xwdtopnm | pnmtopng > GlossAnimate.png"
- file GlossAnimate.png

- time cabal build $CABAL_FLAGS exe:GlossSimulate
- xvfb_runf sh -c "time cabal run $CABAL_FLAGS GlossSimulate & sleep 1 ; xwd -root | xwdtopnm | pnmtopng > GlossSimulate.png"
- file GlossSimulate.png

- time cabal build $CABAL_FLAGS exe:GlossSimulate2
- xvfb_runf sh -c "time cabal run $CABAL_FLAGS GlossSimulate2 & sleep 1 ; xwd -root | xwdtopnm | pnmtopng > GlossSimulate2.png"
- file GlossSimulate2.png

.ch13-3d:
- time cabal build $CABAL_FLAGS exe:VisDisplay
- xvfb_runf sh -c "time cabal run $CABAL_FLAGS VisDisplay & sleep 1 ; xwd -root | xwdtopnm | pnmtopng > VisDisplay.png"
- file VisDisplay.png

.ch13:
- !reference [.ch13-2d]
- !reference [.ch13-3d]

.artifacts:
- GlossDisplay.png
- GlossDisplay2.png
- GlossAnimate.png
- GlossSimulate.png
- GlossSimulate2.png
- VisDisplay.png

.xvfb_runf:
- xvfb_runf () {
    XDG_RUNTIME_DIR=/run/user/$(id -u)
    xvfb-run
        --auto-servernum
        --server-args="-screen 0 1400x900x24 +extension RANDR"
        -- "$@" ; sleep 1 ;
    }


.header-busybox:
- set -o errexit -o nounset -o noglob -o pipefail
- (. /etc/os-release ; echo $PRETTY_NAME)

.header:
- shopt -s failglob
- !reference [.header-busybox]

.ghcup:
- time curl
        --proto '=https'
        --tlsv1.2
        --silent
        --show-error
        -- fail
    https://get-ghcup.haskell.org
    > get-ghcup.sh
- time sh get-ghcup.sh
- . $HOME/.ghcup/env
- ( set -x ; time ghcup install ghc ${GHC_VERSION:-} --set)
- time ghcup install cabal
- ghcup list

.apk:
- !reference [.header-busybox]
- time ${SUDO:-} apk add
        --no-cache
        --quiet
    binutils-gold
    file
    freeglut-dev
    gcc
    g++
    gmp-dev
    libc-dev
    libffi-dev
    make
    mesa-dri-gallium
    musl-dev
    netpbm
    xvfb-run
    xwd
    zlib-dev
    # coreutils
    # libffi-dev
    # zlib-dev for Hackage zlib (servant)

.apk-ghc:
- echo ${ALPINE_RECOMMENDED_VERSION:+:alpine$ALPINE_RECOMMENDED_VERSION}
- !reference [.apk]
- time ${SUDO:-} apk add
        --no-cache
        --quiet
    cabal
    ghc
- ghc --version
- cabal --version
- CABAL_FLAGS="$CABAL_FLAGS
    ${JOB_CABAL_FLAGS:-}"
- printenv CABAL_FLAGS
- CABAL_REPL_FLAGS="$CABAL_REPL_FLAGS
    ${JOB_CABAL_FLAGS:-}"

.apk-ghcup:
- !reference [.apk]
- time apk add
        --no-cache
        --quiet
    curl
    ncurses-dev
    perl
    tar
    xz
- !reference [.ghcup]

.apt:
- !reference [.header]
- time ${SUDO:-} apt-get update --quiet=2
- time ${SUDO:-} apt-get install
        --no-install-suggests
        --no-install-recommends
        --quiet=2
    apt-utils
    moreutils
    > /dev/null
- echo ${GHC_PACKAGES:-}
- time ${CHRONIC:-} ${SUDO:-} apt-get install
        --no-install-suggests
        --no-install-recommends
        --quiet=2
    cabal-install
    file
    g++
    ghc
    netpbm
    time
    x11-apps
    xauth
    xvfb
    zlib1g-dev
    ${GHC_PACKAGES:-}
- ghc --version
- cabal --version
- CABAL_FLAGS="$CABAL_FLAGS
    ${JOB_CABAL_FLAGS:-}"
- printenv CABAL_FLAGS
- CABAL_REPL_FLAGS="$CABAL_REPL_FLAGS
    ${JOB_CABAL_FLAGS:-}"

.dnf:
- !reference [.header]
# - time dnf makecache --timer
- time ${SUDO:-} microdnf install
        --assumeyes
        --quiet
    moreutils > /dev/null
        # --color always
- CHRONIC=chronic
- echo ${GHC_PACKAGES:-}
- time ${CHRONIC:-} ${SUDO:-} microdnf install
        --assumeyes
        --quiet
    binutils
    binutils-gold
    cabal-install
    g++
    gcc
    ghc${GHC_VERSION:-}
    time
    ${GHC_PACKAGES:-}
- ( set -x ;
    ghc${GHC_VERSION:+-$GHC_VERSION} 
        --version
    )
- cabal --version
- CABAL_FLAGS="$CABAL_FLAGS
    ${GHC_VERSION:+--with-compiler=ghc-$GHC_VERSION}
    ${JOB_CABAL_FLAGS:-}"
- printenv CABAL_FLAGS
- CABAL_REPL_FLAGS="$CABAL_REPL_FLAGS
    ${GHC_VERSION:+--with-compiler=ghc-$GHC_VERSION}
    ${JOB_CABAL_FLAGS:-}"

.zypp:
- !reference [.header]
- ${SUDO:-} ${CHRONIC:-} zypper
        --quiet
        --color
        install
        --no-confirm
    cabal-install
    gcc-c++
    ghc
    time
    # ghc-fmt
- ghc --version
- cabal --version
- haddock --version


build-alpine-edge-ghcup:
    script:
    - time cabal build $CABAL_FLAGS
    - time cabal clean
    before_script:
    - !reference [.apk-ghcup]
    - time cabal update
    cache:
        key: recommended
        paths:
        - .cache
        - .local/state
        when: always
    when: manual
    image: alpine:edge

ch13-alpine-edge-ghcup-spot:
    script:
    - !reference [.xvfb_runf]
    - !reference [.ch13]
    before_script:
    - !reference [.apk-ghcup]
    - time cabal update
    artifacts:
        paths: !reference [.artifacts]
    cache:
        key: recommended
        paths:
        - .cache
        - .local/state
        policy: pull
    when: manual
    image: alpine:edge

ch13-alpine-edge-ghc:
    allow_failure: true
    script:
    - !reference [.xvfb_runf]
    - !reference [.ch13]
    before_script:
    - !reference [.apk-ghc]
    - time cabal update
    artifacts:
        paths: !reference [.artifacts]
    cache:
        key: alpine-edge-ghc
        paths:
        - .cache
        - .local/state
        when: always
    when: manual
    image: alpine:edge

ch13-2d-debian:
    allow_failure: true
    script:
    - !reference [.xvfb_runf]
    - !reference [.ch13-2d]
    before_script:
    - !reference [.apt]
    - time cabal update
    variables:
        GHC_PACKAGES:
            libghc-gloss-dev
    artifacts:
        paths: !reference [.artifacts]
    when: manual
    image: debian:unstable-slim


# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/#customization
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
stages:
- test
sast:
    stage: test
include:
- template: Security/SAST.gitlab-ci.yml
